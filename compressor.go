package xtractor

import (
	"archive/tar"
	"archive/zip"
	"compress/gzip"
	"io"
	"io/fs"
	"io/ioutil"
	"os"
	"path/filepath"
	"strings"
)

const (
	Zip      = "zip"
	TarGz    = "tar.gz"
	TarBzip2 = "tar.bz2"

	Written = "files written"
	Read    = "files read"
)

type PrintableProgress interface {
	PrintProgress(howMany int, task string)
}

var progressPrinter PrintableProgress

func getFiles(path string) []file {

	if fileInfo, err := os.Stat(path); err == nil {
		if fileInfo.IsDir() {
			var files []file
			counter := 0
			filepath.WalkDir(path, func(p string, d fs.DirEntry, err error) error {
				if f, e := os.Stat(p); e == nil && !f.IsDir() {
					name := strings.Replace(p, path, "", 1)
					files = append(files, file{name: name, path: path+name})
					progressPrinter.PrintProgress(counter, Read)
					counter++
					return nil
				} else {
					return e
				}
			})
			return files
		}
	}
	return nil
}

func archiveToZip(path string, out io.Writer) {
	zipWriter := zip.NewWriter(out)
	files := getFiles(path)
	for i, file := range files {
		f, e := zipWriter.Create(file.name)
		checkError(e)
		r, err := ioutil.ReadFile(file.path)
		f.Write(r)
		checkError(err)
		progressPrinter.PrintProgress(i, Written)
	}
	zipWriter.Close()
}

func archiveToTarGzip(path string, out io.Writer) {
	g := gzip.NewWriter(out)
	defer g.Close()
	t := tar.NewWriter(g)
	defer t.Close()
	files := getFiles(path)

	for i, file := range files {
		f, err := os.Open(file.path)
		checkError(err)
		defer f.Close()

		
		info, err := f.Stat()
		checkError(err)

		header, err := tar.FileInfoHeader(info, info.Name())
		checkError(err)

		header.Name = f.Name()

		
		err = t.WriteHeader(header)
		checkError(err)

		_, err = io.Copy(t, f)
		checkError(err)

		progressPrinter.PrintProgress(i, Written)
	}
}

func Compress(sourcePath, destDir, name, outFileType string, p PrintableProgress) {
	progressPrinter = p
	out, e := os.Create(filepath.Join(destDir, name+"."+outFileType))
	defer checkError(out.Close())
	checkError(e)

	if outFileType == Zip {
		archiveToZip(sourcePath, out)
	} else if outFileType == TarGz {
		archiveToTarGzip(sourcePath,out)
	}

}

func checkError(e error) {
	if e != nil {
		panic(e)
	}
}
