package xtractor

import (
	"fmt"
	"os"
	"testing"
)

type PrintProgress struct{}

func (p PrintProgress) PrintProgress(howmany int, task string) {
	fmt.Printf("\r%d %s", howmany, task)
}

func Test_archiveToZip(t *testing.T) {
	progressPrinter = PrintProgress{}
	type args struct {
		path string
	}
	tests := []struct {
		name string
		args args
	}{
		{name: "Test", args: args{path: "testdata"}},
	}
	for _, tt := range tests {
		fmt.Println(getFiles(tt.args.path))
		t.Run(tt.name, func(t *testing.T) {
			out, e := os.Create("testdata/test.zip")
			checkError(e)
			archiveToZip(tt.args.path, out)
		})
	}
}

func Test_archiveToTarGzip(t *testing.T) {
	progressPrinter = PrintProgress{}
	t.Run("test",func(t *testing.T) {
		out, e := os.Create("testdata/test.tar.gz")
		checkError(e)
		archiveToTarGzip("testdata", out)
	})
}
